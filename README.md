# Koukei

Local web interface for basic Linux monitoring.

For using, simply execute:

```
./koukei.sh
```

If you want to use your key ID, write it on the `koukei.sh` file. I'm using some images I like, feel free to use your own, it's LOCAL not ONLINE, so once you execute it only you will be able to see the interface. 

Write in `rememberme` the notes you want to have fast access to.

Enjoy.
