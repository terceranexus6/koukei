#!/bin/bash

#everytime we launch the monitor we copy the template and rewrite monitor
cp template.html monitor.html

#commands
procesos=$(ps)
clave_id=<YOUR ID>
date_t=$(date)
prev=$(head -c 20 rememberme)
recuer=$(cat rememberme)
key=$(gpg --list-keys)
freem=$(free -m)
cpu=$(iostat -c)
devi=$(iostat -d)
netw=$(iostat -h)



#replace them in the monitor
rpl --quiet "_PROC_" "$procesos" monitor.html
rpl --quiet "_USERNOW_" "$USER" monitor.html
rpl --quiet "_HORADIA_" "$date_t" monitor.html
rpl --quiet "_IDCLAVE_" "$clave_id" monitor.html
rpl --quiet "_PREVREC_" "$prev" monitor.html
rpl --quiet "_CLAVEPUB_" "$key" monitor.html
rpl --quiet "_FREEM_" "$freem" monitor.html
rpl --quiet "_RECUER_" "$recuer" monitor.html
rpl --quiet "_CPUU_" "$cpu" monitor.html
rpl --quiet "_MYPC_" "$HOSTNAME" monitor.html


#launch the monitor in firefox
firefox monitor.html

